This snippet is a definition of a memoizing fixpoint combinator
following the interface used in the introductory example of the paper

> A Practical Mode System for Recursive Definitions  
> Alban Reynaud, Gabriel Scherer, Jeremy Yallop, 2021  
> https://arxiv.org/abs/1811.08134

The definition is as follows:

```ocaml
let memo_fix open_f =
  let rec f x = open_f (remember memo_table) x
  and memo_table = { f = f; table = new_table () }
  in f
```

(See [memo_fix.ml](memo_fix.ml) for the auxiliary definitions and an example.)

This answers a question (probably?) asked by Jules Jacobs during
a virtual POPL conference, of whether the combinator (as opposed to
a fixed recursive definition) would also fall in the fragment
combining recursive functions or recursive records, or would require
a different recursion pattern.
