(* a definition of the usual fixpoint combinator *)
let fix open_f =
  let rec f x = open_f f x in f

(* this is somewhat slow to compute *)
let _ =
  fix
    (fun pow2 n -> if n = 0 then 1 else pow2 (n - 1) + pow2 (n - 1))
    27

(* buildup for memoization in the style of the introduction *)
type ('a, 'b) memo_table = ('a, 'b) Hashtbl.t
let new_table () = Hashtbl.create 42

type ('a, 'b) memo_structure = {
  f: ('a -> 'b);
  table: ('a, 'b) memo_table;
}
let remember {f; table} a =
  try Hashtbl.find table a
  with Not_found ->
    let b = f a in
    Hashtbl.add table a b;
    b

(* memoizing fixpoint combinator *)
let memo_fix open_f =
  let rec f x = open_f (remember memo_table) x
  and memo_table = { f = f; table = new_table () }
  in f


(* this is instantaneous *)
let _ =
  memo_fix
    (fun pow2 n -> if n = 0 then 1 else pow2 (n - 1) + pow2 (n - 1))
    27
